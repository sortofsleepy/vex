#include "systems/SParticleSystem.hpp"
using namespace ci;
using namespace std;

SParticleSystem::SParticleSystem(){
    setup();
    loadShaders();
    setParticleShape();
}



void SParticleSystem::update(){
    buffer.update([=]()->void{
        gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffer.getDrawBuffer());
    });
}

void SParticleSystem::draw(vec3 eyePos){
    gl::ScopedDepth d(true);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    
    buffer.bind();
    gl::pushMatrices();
    gl::setDefaultShaderVars();
    mParticleBatch->getGlslProg()->uniform("time",(float)app::getElapsedSeconds());
    mParticleBatch->getGlslProg()->uniform("eyeDir",eyePos);
    mParticleBatch->drawInstanced(nParticles);
    gl::popMatrices();
}


void SParticleSystem::loadShaders(){
    try {
        
        // Create a vector of Transform Feedback "Varyings".
        // These strings tell OpenGL what to look for when capturing
        // Transform Feedback data. For instance, Position, Velocity,
        // and StartTime are variables in the updateSmoke.vert that we
        // write our calculations to.
        std::vector<std::string> transformFeedbackVaryings( 6 );
        transformFeedbackVaryings[0] = "Position";
        transformFeedbackVaryings[1] = "Velocity";
        transformFeedbackVaryings[2] = "Phi";
        transformFeedbackVaryings[3] = "Theta";
        transformFeedbackVaryings[4] = "ThetaSpeed";
        transformFeedbackVaryings[5] = "PhiSpeed";
        
        ci::gl::GlslProg::Format mUpdateParticleGlslFormat;
        // Notice that we don't offer a fragment shader. We don't need
        // one because we're not trying to write pixels while updating
        // the position, velocity, etc. data to the screen.
        mUpdateParticleGlslFormat.vertex( app::loadAsset( "soundsphere/soundsphereu.glslv" ) )
        // This option will be either GL_SEPARATE_ATTRIBS or GL_INTERLEAVED_ATTRIBS,
        // depending on the structure of our data, below. We're using multiple
        // buffers. Therefore, we're using GL_SEPERATE_ATTRIBS
        .feedbackFormat( GL_INTERLEAVED_ATTRIBS )
        // Pass the feedbackVaryings to glsl
        .feedbackVaryings( transformFeedbackVaryings )
        .attribLocation( "VertexPosition", PositionIndex )
        .attribLocation( "VertexVelocity", VelocityIndex )
        .attribLocation( "VertexPhi", PhiIndex)
        .attribLocation( "VertexTheta", ThetaIndex)
        .attribLocation( "VertexPhiSpeed", PhiSpeedIndex)
        .attribLocation( "VertexThetaSpeed", ThetaSpeedIndex);
        updateShader = ci::gl::GlslProg::create( mUpdateParticleGlslFormat );
    }catch( const ci::gl::GlslProgCompileExc &ex ) {
        
        CI_LOG_E(ex.what());
    }
         
         
         buffer.setUpdateShader(updateShader);
         //buffer.setRenderShader(mPRenderGlsl);
}
void SParticleSystem::setParticleShape(){
    vector<vec3> positions,normals;
    vector<uint32_t> indices;
    positions.push_back(vec3(0.5,0.5,0.5));
    positions.push_back(vec3(-0.5,-0.5,0.5));
    positions.push_back(vec3(-0.5,0.5,-0.5));
    positions.push_back(vec3(0.5,-0.5,-0.5));
    
    indices.push_back(2);
    indices.push_back(1);
    indices.push_back(0);
    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(2);
    indices.push_back(1);
    indices.push_back(3);
    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);
    indices.push_back(1);
    
   
     
       gl::VboMesh::Layout layout;
       layout.attrib(geom::POSITION,3);
       gl::VboMeshRef mesh = gl::VboMesh::create(positions.size(),GL_TRIANGLES,{layout},indices.size(),GL_UNSIGNED_INT);
       mesh->bufferAttrib(geom::POSITION,sizeof(vec3) * positions.size(),positions.data());
       mesh->bufferIndices(sizeof(uint32_t) * indices.size(),indices.data());
       // setup the buffer
       // create the VBO which will contain per-instance (rather than per-vertex) data
       system = buffer.getBuffer(0);
       
       geom::BufferLayout instanceDataLayout;
       instanceDataLayout.append( geom::CUSTOM_0, 3, sizeof(Particle), offsetof(Particle, position ), 1 );
       //buffer Particle data onto your instanced mesh
       system->bufferSubData(0,sizeof(Particle) * particles.size(), particles.data());
       // now add it to the VboMesh we already made of the Teapot
       mesh->appendVbo( instanceDataLayout, system );
       
       //build out a rendering shader
       gl::GlslProg::Format fmt;
       fmt.vertex(app::loadAsset("soundsphere/SSphereParticles.glslv"));
       fmt.fragment(app::loadAsset("soundsphere/SSphereParticles.glslf"));
       gl::GlslProgRef render = gl::GlslProg::create(fmt);
       //gl::GlslProgRef render = gl::getStockShader(gl::ShaderDef().lambert());
       
       //build out the batch object with a reference to our instanced attribute
       mParticleBatch = gl::Batch::create( mesh, render, {
             { geom::Attrib::CUSTOM_0, "VertexPosition" }
       } );
}

void SParticleSystem::setup(){
    
    nParticles = 900;
    float time = 0.0f;
    float rate = 0.001f;
    particles.reserve(nParticles);
    const float azimuth = 256.0f * M_PI / nParticles;
    const float inclination = M_PI / nParticles;
    const float radius = 440.0f;
    for(int i = 0; i < nParticles;++i){
        
        float x = radius * sin( inclination * i ) * cos( azimuth * i );
        float y = radius * cos( inclination * i );
        float z = radius * sin( inclination * i ) * sin( azimuth * i );
        Particle p;
        p.position = vec3(x,y,z);
        p.velocity = ci::randVec3() * mix( 0.0f, 1.5f, mRand.nextFloat() );
        p.phiSpeed = randFloat(-0.5,0.5);
        p.thetaSpeed = randFloat(-0.5,0.5);
        p.theta = M_PI * randFloat() * 2;
        p.phi = M_PI * randFloat() * 2;
        particles.push_back(p);
        time += rate;
    }
    
    buffer.setData(particles, [=]()->void{
        
        gl::vertexAttribPointer( PositionIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, position) );
        gl::enableVertexAttribArray(PositionIndex);
    
        gl::vertexAttribPointer( VelocityIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, velocity) );
        gl::enableVertexAttribArray(VelocityIndex);
        
        gl::vertexAttribPointer(PhiIndex,1,GL_FLOAT,GL_FALSE,sizeof(Particle),(const GLvoid*) offsetof(Particle,phi));
        gl::enableVertexAttribArray(PhiIndex);
        
        gl::vertexAttribPointer(ThetaIndex,1,GL_FLOAT,GL_FALSE,sizeof(Particle),(const GLvoid*) offsetof(Particle,theta));
        gl::enableVertexAttribArray(ThetaIndex);
        
        gl::vertexAttribPointer( PhiSpeedIndex, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, phiSpeed) );
        gl::enableVertexAttribArray(PhiSpeedIndex);
        
        gl::vertexAttribPointer(ThetaSpeedIndex,1,GL_FLOAT,GL_FALSE,sizeof(Particle),(const GLvoid*) offsetof(Particle,thetaSpeed));
        gl::enableVertexAttribArray(ThetaSpeedIndex);
        
    });
}