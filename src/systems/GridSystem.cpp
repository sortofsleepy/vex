#include "systems/GridSystem.hpp"
using namespace ci;
using namespace std;

GridSystem::GridSystem(int width,int height, int resolution){
    
    float cols = width / resolution;
    float rows = height / resolution;
    
    vector<vec3> positions;
    
    for(int i = 0; i < cols; ++i){
        for(int j = 0; j < rows;++j){
            
            float x = i * resolution;
            float y = j * resolution;
            
            positions.push_back(vec3(x,y,0));
            
        }
    }
    
    layout.attrib(geom::POSITION,3);
    mMesh = gl::VboMesh::create(positions.size(),GL_POINTS,{layout});
    mMesh->bufferAttrib(geom::POSITION,sizeof(vec3) * positions.size(),positions.data());
    
}
