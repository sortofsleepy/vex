#include "post/MotionBlur.hpp"
using namespace ci;
using namespace std;

MotionBlur::MotionBlur(int width,int height){
    sceneIndex = 0;
    accumIndex = 1;
    this->width = width;
    this->height = height;
}

void MotionBlur::setup(){
    gl::Texture::Format fmt;
    fmt.wrap(GL_CLAMP_TO_EDGE);
    fmt.setMagFilter(GL_NEAREST);
    fmt.setMinFilter(GL_NEAREST);
    
    gl::Fbo::Format ffmt;
    ffmt.attachment(GL_COLOR_ATTACHMENT0 + sceneIndex, gl::Texture2d::create(width,height,fmt));
    ffmt.attachment(GL_COLOR_ATTACHMENT0 + accumIndex, gl::Texture2d::create(width,height,fmt));
    mFbo = gl::Fbo::create(width,height,ffmt);
  
    clean();
}