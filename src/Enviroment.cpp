#include "objects/Enviroment.hpp"
#include "core.hpp"
using namespace ci;
using namespace std;

Enviroment::Enviroment():
domeRadius(200.0),
roughness(0.05),
specular(1.0),
exposure(3.0),
metallic(1.0),
gamma(2.2),
baseColor(vec3(1.0,1.0,1.0)){
    
    setupPbr();
}

void Enviroment::setupPbr(){
    auto cubeMapFormat	= gl::TextureCubeMap::Format().mipmap().internalFormat( GL_RGB16F ).minFilter( GL_LINEAR_MIPMAP_LINEAR ).magFilter( GL_LINEAR );
    mIrradianceMap = gl::TextureCubeMap::create(loadImage(app::loadAsset("enviroment/stars_env.jpg")),cubeMapFormat);
    mRadianceMap = gl::TextureCubeMap::createFromDds(app::loadAsset("enviroment/studio_radiance.dds"),cubeMapFormat);
    mGradientTexture = gl::Texture::create(loadImage(loadAsset("enviroment/gradient.png")));
    mEnvTex = gl::Texture::create(loadImage(loadAsset("enviroment/stars.jpg")));
       
}



void Enviroment::draw(){
    mocha::disableDepth();
    gl::ScopedTextureBind tex0(mRadianceMap,0);
    gl::ScopedTextureBind tex1(mIrradianceMap,1);
    gl::ScopedTextureBind tex3(mEnvTex,2);
    gl::ScopedTextureBind tex4(mGradientTexture,3);
    mEnvShader->uniform("uRadianceMap",0);
    mEnvShader->uniform("uIrradianceMap",1);
    mEnvShader->uniform("uEnvTex",2);
    mEnvShader->uniform("uGradientTex",3);
    mEnvShader->uniform("uBaseColor",baseColor);
    mEnvShader->uniform("uSpecular",specular);
    mEnvShader->uniform("uGamma",gamma);
    mEnvShader->uniform("uExposure",exposure);
    mEnvShader->uniform("uRoughness",roughness);
    mEnvShader->uniform("uRoughness4",(float)pow(roughness,4.0));
    mEnvShader->uniform("uMetallic",metallic);

    mEnvBatch->draw();
   
}

void Enviroment::setup(){
    
    const float num = 60.0;
       vector<ci::vec3> positions;
       vector<ci::vec3> normals;
       vector<ci::vec3> centers;
       vector<ci::vec2> uvs;
       vector<uint32_t> indices;
       int count = 0;
       const float uvGap = 1 / num;
           
           
       
       for(int j = 0; j < num; ++j){
           for(int i = 0; i < num ; ++i){
               vec3 v0 = getPosition(i, j);
               vec3 v1 = getPosition(i+1, j);
               vec3 v2 = getPosition(i+1, j+1);
               vec3 v3 = getPosition(i, j+1);
               vec3 n = getNormal(v0, v1, v3);
               vec3 c = getCenter(v0, v2);
               
               positions.push_back(v0);
               positions.push_back(v1);
               positions.push_back(v2);
               positions.push_back(v3);
               
               normals.push_back(n);
               normals.push_back(n);
               normals.push_back(n);
               normals.push_back(n);
               
               centers.push_back(c);
               centers.push_back(c);
               centers.push_back(c);
               centers.push_back(c);
               
               uvs.push_back(vec2(i / num, j / num));
               uvs.push_back(vec2(i / num + uvGap, j / num));
               uvs.push_back(vec2(i / num + uvGap, j / num + uvGap));
               uvs.push_back(vec2(i / num, j / num + uvGap));
               indices.push_back(count * 4 + 0);
               indices.push_back(count * 4 + 1);
               indices.push_back(count * 4 + 2);
               indices.push_back(count * 4 + 0);
               indices.push_back(count * 4 + 2);
               indices.push_back(count * 4 + 3);
    
               
               count++;
           }
       }
    
       gl::VboMesh::Layout layout;
       layout.attrib(geom::POSITION,3);
       layout.attrib(geom::TEX_COORD_0,2);
       layout.attrib(geom::NORMAL,3);
       layout.attrib(geom::CUSTOM_0,3);
     
        
       mEnvMesh = gl::VboMesh::create(positions.size(),GL_TRIANGLES,{layout},indices.size(),GL_UNSIGNED_INT);
       mEnvMesh->bufferAttrib(geom::POSITION,sizeof(vec3) * positions.size(),positions.data());
       mEnvMesh->bufferAttrib(geom::NORMAL,sizeof(vec3) * normals.size(),normals.data());
       mEnvMesh->bufferAttrib(geom::TEX_COORD_0,sizeof(vec2) * uvs.size(),uvs.data());
       mEnvMesh->bufferAttrib(geom::CUSTOM_0,sizeof(vec3) * centers.size(),centers.data());
       mEnvMesh->bufferIndices(sizeof(uint32_t) * indices.size(),indices.data());
       
       //mEnvMesh = gl::VboMesh::create(geom::Sphere().radius(50.0));
       mEnvShader = mocha::loadShader("enviroment/enviroment.glslv","enviroment/enviroment.glslf");
       mEnvBatch = gl::Batch::create(mEnvMesh,mEnvShader,{
           {geom::CUSTOM_0,"center"}
       });
}
ci::vec3 Enviroment::getCenter(ci::vec3 p0, ci::vec3 p1){
    auto x = (p0.x + p1.x) / 2.0;
    auto y = (p0.y + p1.y) / 2.0;
    auto z = (p0.z + p1.z) / 2.0;

    return vec3(x,y,z);
}
ci::vec3 Enviroment::getPosition(int i, int j){
    float num = 60.0;
    const float PI = 3.141592653589793;
    vec3 pos = vec3(0.0);
    float ry = (i/num) * PI * 2.0;
    float rx = (j/num) * PI - (PI/2.0);
    
    pos.y = sin(rx) * domeRadius;
    auto r = cos(rx) * domeRadius;
    pos.x = cos(ry) * r;
    pos.z = sin(ry) * r;
    
    return pos;
}
vec3 Enviroment::getNormal(ci::vec3 p0,ci::vec3 p1, ci::vec3 p2){
    auto pp0 = p0;
    auto pp1 = p1;
    auto pp2 = p2;
    auto v0 = vec3();
    auto v1 = vec3();
    auto n = vec3();
    
    v0 = pp1 - pp0;
    v1 = pp2 - pp0;
    n = glm::cross(v1,v0);
    n = glm::normalize(n);
    return n;
}