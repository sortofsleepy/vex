#include "objects/SoundSphere.hpp"
#include "cinder/ImageIo.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace std;

SoundSphere::SoundSphere():
subdivisions(5.0),
scale(8.9),
phi(0.0),
theta(0.0),
orbitRadius(20.0),
shaderSet(false){
    star.setup().setShader("post/Blur.glslf");
    sphere.setup().setShader("post/Blur.glslf");
    
    phiSpeed = randFloat(-0.05,0.05);
    thetaSpeed = randFloat(-0.05,0.05);
    
}


void SoundSphere::draw(ArcCamRef camera){
    
    phi += phiSpeed * 0.4;
    theta += thetaSpeed * 0.4;
    
    gl::ScopedDepth depth(true);
    gl::ScopedBlendAdditive blend;
    gl::ScopedTextureBind tex0(glowTex,0);
    gl::ScopedTextureBind tex1(spectrum,1);
    gl::ScopedTextureBind tex2(starsMap,2);
    gl::ScopedTextureBind tex3(corona,3);
    gl::ScopedTextureBind tex4(mRadianceMap,4);
    gl::ScopedModelMatrix scopedModel;
    
    float x = cos(theta) * sin(phi) * orbitRadius;
    float y = sin(theta) * sin(phi) * orbitRadius;
    float z = cos(phi) * orbitRadius;
    
    gl::pushMatrices();

    star.bind();
    gl::clear();
    camera->useMatrices();
    gl::translate(vec3(x,y,z));
       
    system.update();
    system.draw(camera->getEye());
    star.unbind();
    
    // start sphere drawing
    sphere.bind();
    gl::clear();
    camera->useMatrices();
 
    if(shaderSet){
        gl::pushMatrices();
        gl::translate(vec3(x,y,z));
        gl::scale(vec3(scale));
        
        mBatch->getGlslProg()->uniform("glowTex",0);
        mBatch->getGlslProg()->uniform("time",(float)app::getElapsedSeconds());
        mBatch->getGlslProg()->uniform("spectrum",1);
        mBatch->getGlslProg()->uniform("starsMap",2);
        mBatch->getGlslProg()->uniform("starRadius",4.0f);
        mBatch->getGlslProg()->uniform("eyePos",camera->getEye());
        mBatch->draw();
        gl::popMatrices();
    }
    
    // draw coronal ring
      gl::pushMatrices();
      gl::rotate(camera->getOrientation());
      gl::translate(vec3(x,y,z + 2.0));
      gl::scale(vec3(1.002));
      mCoronaBatch->getGlslProg()->uniform("coronaTex",3);
      mCoronaBatch->getGlslProg()->uniform("spectrumTex",1);
      mCoronaBatch->getGlslProg()->uniform("starColor",0.6f);
      mCoronaBatch->getGlslProg()->uniform("power",1.0f);
      mCoronaBatch->draw();
      
      // draw outer shell
      mOuterBatch->getGlslProg()->uniform("uRadianceMap",4);
      mOuterBatch->getGlslProg()->uniform("uIrradianceMap",2);
      mOuterBatch->getGlslProg()->uniform("uEnvTex",6);
      //mOuterBatch->draw();
     gl::popMatrices();
    sphere.unbind();
    gl::popMatrices(); // end drawing of everything
    // end sphere drawing
    
    star.processFx();
    sphere.processFx();
    camera->orthoMatrices();
    mocha::disableDepth();
     sphere.draw(1);
     star.draw(1);
     
     
 
   
}
//==============
void SoundSphere::setup(){
    starsMap = gl::TextureCubeMap::create(loadImage(app::loadAsset("enviroment/stars_env.jpg")));
    glowTex = gl::Texture::create(loadImage(loadAsset("textures/glow.png")));
    spectrum = gl::Texture::create(loadImage(loadAsset("textures/spectrum.png")));
    corona = gl::Texture::create(loadImage(loadAsset("textures/corona.png")));
    
    setupOuter();
    
    // start by copying the base icosahedron in its entirety (vertices are shared among faces)
    mPositions.assign( reinterpret_cast<vec3*>(SoundSphere::sPositions), reinterpret_cast<vec3*>(SoundSphere::sPositions) + 12 );
    mNormals.assign( reinterpret_cast<vec3*>(SoundSphere::sPositions), reinterpret_cast<vec3*>(SoundSphere::sPositions) + 12 );
    mIndices.assign( SoundSphere::sIndices, SoundSphere::sIndices + 60 );
    
    explodeModifier();
    subdivide();
    // spherize
    for( auto &pos : mPositions )
        pos = normalize( pos );
    
    for( auto &normal : mNormals )
        normal = normalize( normal );
    
    // add color if necessary
    size_t numPositions = mPositions.size();
    mColors.resize( numPositions );
    for( size_t i = 0; i < numPositions; ++i ) {
        
        mColors[i].x = mPositions[i].x * 0.5f + 0.5f;
        mColors[i].y = mPositions[i].y * 0.5f + 0.5f;
        mColors[i].z = mPositions[i].z * 0.5f + 0.5f;
    }
    
    // calculate texture coords based on equirectangular texture map
    calculateImplUV();
    
    layout.attrib(geom::POSITION,3);
    layout.attrib(geom::NORMAL,3);
    layout.attrib(geom::TEX_COORD_0,2);
    
    mMesh = gl::VboMesh::create(mPositions.size(),GL_TRIANGLES,{layout},mIndices.size(),GL_UNSIGNED_INT);
    mMesh->bufferAttrib(geom::POSITION,sizeof(vec3)  * mPositions.size(),mPositions.data());
    mMesh->bufferAttrib(geom::NORMAL,sizeof(vec3) * mNormals.size(),mNormals.data());
    mMesh->bufferIndices(sizeof(uint32_t) * mIndices.size(),mIndices.data());
    
    buildCorona();
       
}

void SoundSphere::explodeModifier(){
    vector<vec3> vertices;
    vector<uint32_t> indices;
    for(int i = 0; i < mIndices.size();i += 3){
        auto n = vertices.size();
        
        auto a = mIndices[i];
        auto b = mIndices[i + 1];
        auto c = mIndices[i + 2];
        
        auto va = mPositions[a];
        auto vb = mPositions[b];
        auto vc = mPositions[c];
        
        vertices.push_back(va);
        vertices.push_back(vb);
        vertices.push_back(vc);
        
        indices.push_back(n);
        indices.push_back(n + 1);
        indices.push_back(n + 2);
    }
    
   
    // rebuild positions and indices
    mPositions.clear();
    for(vec3 v : vertices){
        mPositions.push_back(v);
    }
    
    mIndices.clear();
    for(uint32_t v : indices){
        mIndices.push_back(v);
    }
    
}

void SoundSphere::buildCorona(){
   auto geo = geom::Circle().radius(21.0);
   mCoronaRing = gl::VboMesh::create(geo);
   mCoronaShader = mocha::loadShader("soundsphere/corona.glslv","soundsphere/corona.glslf");
   mCoronaBatch = gl::Batch::create(mCoronaRing,mCoronaShader);
   
}

void SoundSphere::setShader(std::string vertex,string fragment,string geom){
    mShader = mocha::loadShader(vertex,fragment,geom);
    mBatch = gl::Batch::create(mMesh,mShader);
    shaderSet = true;
}

void SoundSphere::setupOuter(){
    auto geo = geom::Sphere().radius(8.0).subdivisions(25.0);
    mOuterSphere = gl::VboMesh::create(geo);
    mOuterShader = mocha::loadShader("soundsphere/outershell.glslv","soundsphere/outershell.glslf");
    mOuterBatch = gl::Batch::create(mOuterSphere,mOuterShader);
    
    auto cubeMapFormat	= gl::TextureCubeMap::Format().mipmap().internalFormat( GL_RGB16F ).minFilter( GL_LINEAR_MIPMAP_LINEAR ).magFilter( GL_LINEAR );
    mIrradianceMap = starsMap;
    mRadianceMap = gl::TextureCubeMap::createFromDds(app::loadAsset("enviroment/studio_radiance.dds"),cubeMapFormat);
    
    
}

void SoundSphere::calculateImplUV(){
    // calculate texture coords
    mTexCoords.resize( mNormals.size(), vec2() );
    for( size_t i = 0; i < mNormals.size(); ++i ) {
        
        const vec3 &normal = mNormals[i];
        mTexCoords[i].x = 0.5f - 0.5f * glm::atan( normal.x, -normal.z ) / float( M_PI );
        mTexCoords[i].y = 1.0f - glm::acos( normal.y ) / float( M_PI );
    }
    // lambda closure to easily add a vertex with unique texture coordinate to our mesh
    auto addVertex = [&] ( size_t i, const vec2 &uv ) {
        const uint32_t index = mIndices[i];
        mIndices[i] = (uint32_t)mPositions.size();
        mPositions.push_back( mPositions[index] );
        mNormals.push_back( mNormals[index] );
        mTexCoords.push_back( uv );
        mColors.push_back( mColors[index] );
    };
    // fix texture seams (this is where the magic happens)
    size_t numTriangles = mIndices.size() / 3;
    for( size_t i = 0; i < numTriangles; ++i ) {
        
        const vec2 &uv0 = mTexCoords[ mIndices[i * 3 + 0] ];
        const vec2 &uv1 = mTexCoords[ mIndices[i * 3 + 1] ];
        const vec2 &uv2 = mTexCoords[ mIndices[i * 3 + 2] ];
    
        const float d1 = uv1.x - uv0.x;
        const float d2 = uv2.x - uv0.x;
        if( math<float>::abs(d1) > 0.5f && math<float>::abs(d2) > 0.5f )
            addVertex( i * 3 + 0, uv0 + vec2( (d1 > 0.0f) ? 1.0f : -1.0f, 0.0f ) );
        else if( math<float>::abs(d1) > 0.5f )
            addVertex( i * 3 + 1, uv1 + vec2( (d1 < 0.0f) ? 1.0f : -1.0f, 0.0f ) );
        else if( math<float>::abs(d2) > 0.5f )
            addVertex( i * 3 + 2, uv2 + vec2( (d2 < 0.0f) ? 1.0f : -1.0f, 0.0f ) );
    }
}

void SoundSphere::subdivide(){
    for( int j = 0; j < subdivisions; ++j ) {
        
    mPositions.reserve( mPositions.size() + mIndices.size() );
    mNormals.reserve( mNormals.size() + mIndices.size() );
    mIndices.reserve( mIndices.size() * 4 );
    const size_t numTriangles = mIndices.size() / 3;
    for( uint32_t i = 0; i < numTriangles; ++i ) {
        
        uint32_t index0 = mIndices[i * 3 + 0];
        uint32_t index1 = mIndices[i * 3 + 1];
        uint32_t index2 = mIndices[i * 3 + 2];
        
        uint32_t index3 = (uint32_t)mPositions.size();
        uint32_t index4 = index3 + 1;
        uint32_t index5 = index4 + 1;
        
        // add new triangles
        mIndices[i * 3 + 1] = index3;
        mIndices[i * 3 + 2] = index5;
        
        mIndices.push_back( index3 );
        mIndices.push_back( index1 );
        mIndices.push_back( index4 );
        mIndices.push_back( index5 );
        mIndices.push_back( index3 );
        mIndices.push_back( index4 );
        
        mIndices.push_back( index5 );
        mIndices.push_back( index4 );
        mIndices.push_back( index2 );
        
        // add new positions
        mPositions.push_back( 0.5f * (mPositions[index0] + mPositions[index1]) );
        mPositions.push_back( 0.5f * (mPositions[index1] + mPositions[index2]) );
        mPositions.push_back( 0.5f * (mPositions[index2] + mPositions[index0]) );
        // add new normals
        mNormals.push_back( 0.5f * (mNormals[index0] + mNormals[index1]) );
        mNormals.push_back( 0.5f * (mNormals[index1] + mNormals[index2]) );
        mNormals.push_back( 0.5f * (mNormals[index2] + mNormals[index0]) );
     }
   }
}


#undef PHI	// take the reciprocal of phi, to obtain an icosahedron that fits a unit cube
#define PHI (1.0f / ((1.0f + math<float>::sqrt(5.0f)) / 2.0f))

float SoundSphere::sPositions[12*3] = { 
	-PHI, 1.0f, 0.0f,    PHI, 1.0f, 0.0f,   -PHI,-1.0f, 0.0f,    PHI,-1.0f, 0.0f,
	0.0f, -PHI, 1.0f,   0.0f,  PHI, 1.0f,   0.0f, -PHI,-1.0f,   0.0f,  PHI,-1.0f,
	1.0f, 0.0f, -PHI,   1.0f, 0.0f,  PHI,  -1.0f, 0.0f, -PHI,  -1.0f, 0.0f,  PHI };

float SoundSphere::sTexCoords[60 * 2] = {
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,

	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,

	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,

	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f };

uint32_t SoundSphere::sIndices[60] ={
	0,11, 5, 0, 5, 1, 0, 1, 7, 0, 7,10, 0,10,11,
	5,11, 4, 1, 5, 9, 7, 1, 8,10, 7, 6,11,10, 2,
	3, 9, 4, 3, 4, 2, 3, 2, 6, 3, 6, 8, 3, 8, 9,
	4, 9, 5, 2, 4,11, 6, 2,10, 8, 6, 7, 9, 8, 1 };