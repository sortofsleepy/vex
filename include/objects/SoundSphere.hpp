#ifndef SoundSphere_hpp
#define SoundSphere_hpp

#include "cinder/gl/Texture.h"
#include "ArcCam.hpp"
#include "Object3D.hpp"
#include "cinder/Vector.h"
#include "cinder/gl/Vbo.h"
#include <vector>
#include <string>
#include "systems/SParticleSystem.hpp"
#include "post/FxPass.hpp"


class SoundSphere : public Object3D{
    mutable std::vector<ci::vec3> mPositions, mNormals, mColors;
    mutable std::vector<ci::vec2> mTexCoords;
    mutable std::vector<uint32_t> mIndices;
    int subdivisions;
    static float sPositions[12*3];
    static float sTexCoords[60*2];
    static uint32_t sIndices[60];
    
    bool shaderSet;
    float scale;
    
    FxPass star,sphere;
    
    // corona
    ci::gl::VboMeshRef mCoronaRing;
    ci::gl::GlslProgRef mCoronaShader;
    ci::gl::BatchRef mCoronaBatch;
    
    // outer sphere
    ci::gl::VboMeshRef mOuterSphere;
    ci::gl::GlslProgRef mOuterShader;
    ci::gl::BatchRef mOuterBatch;
    ci::gl::TextureRef mOuterEnvTex;
   // pbr stuff
   ci::gl::TextureCubeMapRef mIrradianceMap,mRadianceMap;
     
    // orbiting values
    float orbitRadius;
    float theta;
    float phi;
    float thetaSpeed;
    float phiSpeed;
    
    ci::gl::TextureRef glowTex,spectrum,corona;
    ci::gl::TextureCubeMapRef starsMap;
    
    SParticleSystem system;
    void setupOuter();
    void explodeModifier();
    void calculateImplUV();
    void subdivide();
    void buildCorona();
public:
    SoundSphere();
    void setup();
    void draw(ArcCamRef camera);
    void setShader(std::string vertex, std::string fragment="", std::string geom="");
};

#endif