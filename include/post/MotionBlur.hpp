#ifndef MotionBlur_hpp
#define MotionBlur_hpp

#include "post/FxPass.hpp"
class MotionBlur : public FxPass{

    int sceneIndex;
    int accumIndex;
public:
    MotionBlur(int width,int height);
    void setup();
};

#endif