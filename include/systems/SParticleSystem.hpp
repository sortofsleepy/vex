#ifndef SphereSystem_hpp
#define SphereSystem_hpp

#include "cinder/Log.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "FeedbackBuffer.hpp"
#include "cinder/Vector.h"
#include "cinder/Rand.h"
#include <vector>
typedef struct {
    ci::vec3 position;
    ci::vec3 velocity;
   
    float phi;
    float theta;
    float phiSpeed;
    float thetaSpeed;
       
}Particle;
const int PositionIndex	= 0;
const int VelocityIndex = 1;
const int PhiIndex = 2;
const int ThetaIndex = 3;
const int ThetaSpeedIndex = 4;
const int PhiSpeedIndex = 5;

class SParticleSystem {

    FeedbackBuffer buffer;
    //! update shader for TF
    ci::gl::GlslProgRef updateShader;
    
    //! render shader for TF
    ci::gl::GlslProgRef renderShader;
    
    ci::Rand mRand;
    ci::gl::VboRef system;
    int nParticles;
    ci::gl::BatchRef mParticleBatch;
    std::vector<Particle> particles;
    
    void setParticleShape();
    void setup();
    void loadShaders();
public:
    SParticleSystem();
    void update();
    void draw(ci::vec3 eyePos=ci::vec3());
};

#endif