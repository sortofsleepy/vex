#pragma once
#include <vector>
#include "cinder/BSpline.h"
#include "cinder/Vector.h"
using namespace ci;
using namespace std;

typedef struct {
    vector<ci::vec3> positions;
    vector<uint32_t> indices;
}Geometry;

namespace mocha {
    static Geometry CreateTetrahedron(){
        Geometry geo;
        geo.positions.push_back(vec3(1,1,1));
        geo.positions.push_back(vec3(-1,-1,1));
        geo.positions.push_back(vec3(-1,1,-1));
        geo.positions.push_back(vec3(1,-1,-1));
        
        geo.indices.push_back(2);
        geo.indices.push_back(1);
        geo.indices.push_back(0);
        geo.indices.push_back(0);
        geo.indices.push_back(3);
        geo.indices.push_back(2);
        geo.indices.push_back(1);
        geo.indices.push_back(3);
        geo.indices.push_back(0);
        geo.indices.push_back(2);
        geo.indices.push_back(3);
        geo.indices.push_back(1);
        
        return geo;
    }
    
    //! builds a series of points in a 2D circle.
    //! Note that this only builds an outer ring and not
    //! a fully triangulated circle so a plain vector is returned.
    static vector<vec3> Circle2D(float radius=30.0){
        vector<vec3> pts;
        const float deg2Rad = M_PI / 180.0;
        for(int i = 0; i < 360;++i){
            float degInRad = i * deg2Rad;
            float x = cos(degInRad) * radius;
            float y = sin(degInRad) * radius;
            pts.push_back(vec3(x,y,0));
        }
        
        return pts;
    }
    
    
    static vector<vec2> ResamplePoints(vector<vec2> points,
                               float degrees=30.0,
                               bool loop=false,
                               bool open=true){
        vector<vec2> sampled;
        auto spline = BSpline2f(points,degrees,loop,open);
        float resolution = 1000.0f;
        float delta = 1.0f / resolution;
        
        for(float theta = 0.0f; theta < 1.0f; theta += delta){
            sampled.push_back(spline.getPosition(theta));
        }
        
        return sampled;
            
    }

    static vector<vec3> ResamplePoints(vector<vec3> points,
                               float degrees=30.0,
                               bool loop=false,
                               bool open=true){
        vector<vec3> sampled;
        auto spline = BSpline3f(points,degrees,loop,open);
        float resolution = 1000.0f;
        float delta = 1.0f / resolution;
        for(float theta = 0.0f; theta < 1.0f; theta += delta){
            sampled.push_back(spline.getPosition(theta));
        }
        
        return sampled;
    }
}