#ifndef ArcCam_hpp
#define ArcCam_hpp

#include "cinder/Camera.h"
#include "cinder/CameraUi.h"
#include "cinder/Timeline.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"

typedef std::shared_ptr<class ArcCam> ArcCamRef;

class ArcCam {
    ci::CameraPersp mCam;
    ci::CameraUi mCamUi;
    ci::Anim<ci::vec2> mMouse;
    ci::vec3 target;
public:
    ArcCam(float fov,float aspect, float near, float far, ci::vec3 eye, ci::vec3 target);
    static ArcCamRef create(float fov=60.0,
                            float aspect=ci::app::getWindowWidth() / ci::app::getWindowHeight(),
                            float near=0.1,
                            float far=10000.0,
                            ci::vec3 eye=ci::vec3(0, 0, 50),
                            ci::vec3 target=ci::vec3(0,0,0)){
        return ArcCamRef(new ArcCam(fov,aspect,near,far,eye,target));   
    }
    
    ci::vec3 getEye(){
        return mCam.getEyePoint();
    }
    void setTarget(ci::vec3 target);
    void setFar(float far);
    void setNear(float near);
    void setZoom(float zoom);
    void rotateView(ci::mat4 rotateMat);
    //! use the matrix based on the perspective camera.
    //! Use with 3d content
    void useMatrices();
    
    //! use orthographic projection instead based on the current window
    //! use with 2d content
    void orthoMatrices();
    
    ci::quat getOrientation(){
        return mCam.getOrientation();
    }
};

#endif