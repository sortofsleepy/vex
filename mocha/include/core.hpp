#pragma once
#include "cinder/Rand.h"
#include "cinder/Log.h"
#include "cinder/app/App.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/gl.h"
#include <string>

//! Stringify macro for quickly generating shaders
#define STRINGIFY(A) #A

using namespace ci::app;
using namespace ci;
using namespace std;

namespace mocha {
    //! loads a shader. Pass in the path to the vertex and fragment shaders(optionally a geometry shader too if desired)
    static gl::GlslProgRef loadShader(string vertex, string fragment="", string geom=""){
        gl::GlslProgRef shader;
        gl::GlslProg::Format fmt;
        
        // check if this is source code or not.
        size_t vcode_found = vertex.find("uniform");
        size_t fcode_found = fragment.find("uniform");
               
        if(vcode_found != string::npos){
            fmt.vertex(vertex);
        }else{
            fmt.vertex(loadAsset(vertex));
        }
        
        fmt.setPreprocessingEnabled(true);
        
        // check for fragment shader. Sometimes may skip fragment shader (IE transform feedback)
        if(fragment != ""){
            if(fcode_found != string::npos){
                fmt.fragment(fragment);
            }else{
                fmt.fragment(loadAsset(fragment));
            }
        }
        
        if(geom != ""){
            fmt.geometry(loadAsset(geom));
        }
        
        try{
            shader = gl::GlslProg::create(fmt);
        }catch(gl::GlslProgExc &e){
            CI_LOG_E(e.what());
        }
        
        return shader;
    }
    
    //! builds a vector of vec3s containing random values.
    //! Pass in a number to determine how many vec3s are created. Default is 100
    static vector<ci::vec3> randomVec3Group(int num=100){
        vector<ci::vec3> positions;
        for(int i = 0; i < num; ++i){
            positions.push_back(randVec3() * vec3(20.0));
        }
        
        return positions;
    }
    
    static void enableDepth(){
        gl::enableDepthRead();
        gl::enableDepthWrite();
    }
    
    static void disableDepth(){
        gl::disableDepthRead();
        gl::disableDepthWrite();
    }
}