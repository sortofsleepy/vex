#ifndef Sonics_hpp
#define Sonics_hpp


#include "cinder/audio/audio.h"
#include "cinder/app/App.h"
#include "cinder/audio/Voice.h"
#include "cinder/audio/Source.h"
#include "cinder/gl/Texture.h"
#include <vector>
#include <string>

typedef std::shared_ptr<class Sonics>SonicsRef;
class Sonics {
    
    //! texture for uploading to shader
    ci::gl::TextureRef mTex;
    ci::gl::Texture::Format fmt;
    
    //! store input audio
    ci::audio::VoiceRef mVoice;
    
    //! graph to grab audio spectrum
    ci::audio::MonitorSpectralNodeRef mMonitorSpectralNode;
    
    //! Holds audio data 
    std::vector<float> mMagSpectrum;
    
    bool initialLoad;
    	
    
public:
    Sonics();
    Sonics(std::string file);
    
    static SonicsRef create(std::string file){
        return SonicsRef(new Sonics(file));
    }
    
    //! draws the texture to ensure data is getting loaded onto it.
    //! make sure to setup an orthographic perspective first.
    void debugDraw();
    
    void update();
    void loadData(std::vector<float> * data,int width=512,int height=512);
    virtual void play();
    //virtual void pause();
    
};

#endif