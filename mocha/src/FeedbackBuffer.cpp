//
//  FeedbackBuffer.cpp
//  InterleaveTest
//
//  Created by Joseph Chow on 10/25/15.
//
//
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#include "FeedbackBuffer.hpp"
using namespace ci;
using namespace std;

FeedbackBuffer::FeedbackBuffer(){}

ci::gl::VboRef* FeedbackBuffer::getBuffers(){
    return buffers;
}

ci::gl::VboRef FeedbackBuffer::getBuffer(int index){
    return buffers[index];
}

void FeedbackBuffer::setUpdateShader(ci::gl::GlslProgRef updateShader){
    this->updateShader = updateShader;
}

void FeedbackBuffer::setRenderShader(ci::gl::GlslProgRef renderShader){
    this->renderShader = renderShader;
}


void FeedbackBuffer::update(const std::function<void()> fn){
    // This equation just reliably swaps all concerned buffers
    mDrawBuff = 1 - mDrawBuff;
    
    gl::ScopedGlslProg	glslScope( updateShader );
    // We use this vao for input to the Glsl, while using the opposite
    // for the TransformFeedbackObj.
    gl::ScopedVao		vaoScope( vaos[mDrawBuff] );
    // Because we're not using a fragment shader, we need to
    // stop the rasterizer. This will make sure that OpenGL won't
    // move to the rasterization stage.
    gl::ScopedState		stateScope( GL_RASTERIZER_DISCARD, true );
    
    updateShader->uniform( "Time", app::getElapsedFrames() / 60.0f );
    
    // bind your buffer with the callback. Using callback for flexibility between interleaved and seperate attributes
    //gl::bindBufferBase( GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffers[1-mDrawBuff] );
    fn();
    
    // We begin Transform Feedback, using the same primitive that
    // we're "drawing". Using points for the particle system.
    gl::beginTransformFeedback( drawType );
    gl::drawArrays( GL_POINTS, 0, numItems );
    gl::endTransformFeedback();	

}

void FeedbackBuffer::setGeometryType(GLenum drawType){
    this->drawType = drawType;
}

void FeedbackBuffer::setDataStructureType(GLenum dataStructureType){
    this->dataStructureType = dataStructureType;
}

void FeedbackBuffer::bind(){

    gl::ScopedVao vao(vaos[1-mDrawBuff]);
    gl::ScopedGlslProg render(renderShader);
    
    gl::setDefaultShaderVars();
}

void FeedbackBuffer::unbind(){}

void FeedbackBuffer::setData(ci::gl::VboRef buffer, int dataSize, std::function<void ()> fn){
    buffers[0] = buffer;
    buffers[1] = buffer;
    
    numItems = dataSize;
    
    for( int i = 0; i < 2; i++ ) {
        // Initialize the Vao's holding the info for each buffer
        vaos[i] = ci::gl::Vao::create();
        
        vaos[i]->bind();
        buffers[i]->bind();
        fn();
    }
}

