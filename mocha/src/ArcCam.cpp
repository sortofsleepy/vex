#include "ArcCam.hpp"
using namespace ci;
using namespace std;
using namespace ci::app;

ArcCam::ArcCam(float fov,float aspect, float near, float far,vec3 eye, vec3 target){
    
    mCam =  CameraPersp( getWindowWidth(), getWindowHeight(), fov, near, far ).calcFraming( Sphere( vec3(0), 200 ) );
    mCam.lookAt(eye,target);
    mCamUi = CameraUi( &mCam, getWindow(), -1 );
    mCam.setAspectRatio(aspect);
    
    this->target = target;
    
        //there are no mouse events on mobile - so only run this if on desktop
    #ifdef CINDER_MAC
        getWindow()->getSignalMouseDown().connect([this](MouseEvent event){
            mMouse.stop();
            mMouse = event.getPos();
            mCamUi.mouseDown( mMouse() );
        });
        
        getWindow()->getSignalMouseDrag().connect([this](MouseEvent event){
            timeline().apply(&mMouse,vec2(event.getPos()),1.0f,EaseOutQuint()).updateFn([=]
                                                                                        {
                                                                                            mCamUi.mouseDrag( mMouse(), event.isLeftDown(), false, event.isRightDown() );
                                                                                        });
        });
    #endif
    
 
}
                                                                                                                                                  


void ArcCam::orthoMatrices(){
    gl::setMatricesWindow(getWindowSize());
    gl::viewport(getWindowSize());
}
void ArcCam::rotateView(ci::mat4 rotateMat){
    
}

void ArcCam::useMatrices(){
    gl::setMatrices(mCam);
}

void ArcCam::setTarget(ci::vec3 target){
    mCam.lookAt(target);
}
void ArcCam::setFar(float far){
    mCam.setFarClip(far);
}
void ArcCam::setNear(float near) {
    mCam.setNearClip(near);
}
void ArcCam::setZoom(float zoom){
    auto currentEye = mCam.getEyePoint();
    currentEye.z = zoom;
    mCam.lookAt(currentEye,target);
}