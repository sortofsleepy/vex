#include "Sonics.hpp"
#include "cinder/Log.h"
#include "cinder/gl/gl.h"
using namespace ci;
using namespace std;
Sonics::Sonics():initialLoad(false){}
Sonics::Sonics(std::string file):initialLoad(false){
    auto ctx = audio::Context::master();
    mVoice = audio::Voice::create(audio::load(app::loadAsset(file)));
    
    auto monitorFormat = audio::MonitorSpectralNode::Format().fftSize( 2048 ).windowSize( 1024 );
    mMonitorSpectralNode = ctx->makeNode( new audio::MonitorSpectralNode( monitorFormat ) );
    
    mVoice->getInputNode() >> mMonitorSpectralNode;
    ctx->enable();
    
    // setup texture format for storing audio data
    fmt.minFilter(GL_NEAREST);
    fmt.magFilter(GL_NEAREST);
    fmt.setWrap(GL_CLAMP_TO_EDGE,GL_CLAMP_TO_EDGE);
    fmt.setDataType(GL_FLOAT);
    
    // initialize texture with some dummy data
    vector<float> data;
    for(int i = 0; i < 100;++i){
        data.push_back(0.0f);
    }
    loadData(&data);
}
void Sonics::loadData(std::vector<float> * data,int width, int height){
    // make sure we initialize texture first. Set flag once that's occured.
    if(!initialLoad){
        mTex = gl::Texture::create(data->data(), GL_RGBA,width,height,fmt);
        initialLoad = true;
    }else{
        // note - mip level is 0 cause there ain't no mip-ing
        mTex->update(data->data(),GL_RGBA,GL_FLOAT,0,width,height);
    }
}
void Sonics::update(){
    // We copy the magnitude spectrum out from the Node on the main thread, once per update:
    mMagSpectrum = mMonitorSpectralNode->getMagSpectrum();
    loadData(&mMagSpectrum);
}
void Sonics::debugDraw(){
    gl::draw(mTex,Rectf(0,0,512,512));
}
void Sonics::play(){
    mVoice->start();
}