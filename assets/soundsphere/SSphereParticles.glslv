#version 150
uniform vec3 eyePos;
uniform mat4 ciModelViewProjection;
uniform mat3 ciNormalMatrix;
uniform float time;
in vec4	ciPosition;
in vec4	ciColor;
in vec3	VertexPosition; // per-instance position variable
#include "../shaders/rotate.glslv"
out vec3 vEyeDir;
out vec3 vPosition;
void main( void )
{
    vec3 rot = rotateX(ciPosition.xyz,time);
    vec4 calcPos = vec4(rot,1.) + vec4(VertexPosition,1.);
    
    calcPos.x *= 0.28;
    calcPos.y *= 0.28;
    calcPos.z *= 0.28;
    
    vPosition = calcPos.xyz;
    vPosition *= 40.0;
    vEyeDir = normalize(eyePos - vPosition);
    vec4 pos = ciModelViewProjection * calcPos;
    
    gl_Position	= pos;
 
}