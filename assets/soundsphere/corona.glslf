#version 150
uniform sampler2D coronaTex;
uniform sampler2D spectrumTex;
uniform float starColor;
uniform float power;

in vec4 vPosition;
in vec2 vUv;

out vec4 glFragColor;
void main()
{
	vec3 spectrumCol	= texture( spectrumTex, vec2( starColor, 0.25 ) ).rgb;
	vec4 coronaCol		= texture( coronaTex, vUv );

	
	float coronaAlpha	= coronaCol.a;
	
	vec3 offColor		= vec3( 0.0 );
	vec3 onColor		= vec3( spectrumCol );
	
	glFragColor.rgb	= mix( offColor, onColor, power );
	glFragColor.a		= coronaAlpha;
}



