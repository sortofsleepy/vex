#version 150
uniform vec3 eyePos;
uniform mat4 ciModelViewProjection;
out float fogAmount;
in vec3 ciPosition;
in vec3 ciNormal;
in vec2 ciTexCoord0;
uniform float time;

out vec2 vUv;
out vec3 vNormal;
out vec3 vPosition;
out vec3 vEyeDir;


#define FOG_START 90
#define FOG_END 200

#include "../shaders/rotate.glslv"

void main(){
    vUv = ciTexCoord0;
    vNormal = ciNormal;
    
    vec3 pos = ciPosition;
    pos = rotateX(pos,time);
    pos = rotateY(pos,time);
    pos = rotateZ(pos,time);
    
    gl_Position = ciModelViewProjection * vec4(pos,1.);
    
    vPosition = ciPosition;
    //vPosition *= 40.0;
    vEyeDir = normalize(eyePos - vPosition);
    
    float fogDistance = length(gl_Position.xyz);
    fogAmount = fogFactorLinear(fogDistance,FOG_START,FOG_END);
}