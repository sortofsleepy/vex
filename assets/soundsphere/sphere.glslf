#version 150
uniform samplerCube starsMap;
uniform float starRadius;
uniform sampler2D spectrum;
uniform sampler2D glowTex;
uniform vec3 envColor;
in vec2 vUv;
in float fogAmount;
in vec3 vPosition;
in vec3 vNormal;
in vec3 vEyeDir;
out vec4 glFragColor;
#define FOG_DENSITY 1010.5
float fog_exp2(const float dist, const float density) {
  const float LOG2 = -1.442695;
  float d = density * dist;
  return 1.0 - clamp(exp2(d * d * LOG2), 0.0, 1.0);
}

const float mainPower = 0.5;
const float color = 0.6;
void main(){
 
   vec4 glow = texture(glowTex,vUv);
   vec3 spectrumCol = texture( spectrum, vUv ).rgb;
   	
   // ====== LIGHTING =====
   vec3 lightPos = vec3( 0.0, 300.0, 0.0 );
   vec3 lightDir = lightPos - vPosition;
   vec3 lightDirNorm = normalize( lightDir );
   
   float ppDiff = max( dot( vNormal, lightDirNorm ), 0.0 );
   vec3 HV = normalize( lightDirNorm + vEyeDir );
   float NdotHV	= max( dot( vNormal, HV ), 0.0 );
   float ppSpec = pow( NdotHV, 30.0 );
   float ppFres = pow( 1.0 - ppDiff, 2.5 );
   float ppEyeDiff = max( dot( vNormal, vEyeDir ), 0.0 );
   float ppEyeFres = pow( 1.0 - ppEyeDiff, 2.0 );
   vec3 c = vec3( 1.0 - ppEyeFres * 0.2 );
   vec3 reflectDir = reflect( vEyeDir, vNormal * vec3( 1.0, 1.5, 1.0 ) );
   
   vec3 envColor = texture( starsMap, reflectDir ).rgb;
   float envGrey = envColor.r;
   float envSpec = envColor.g;
   float fakeAo = pow( vNormal.y * 0.3 + 0.3, 0.4 );
   float rimLight = pow( fakeAo, 4.0 ) * ppEyeFres;
   float bloomShadow = vPosition.y * 0.005 + 0.8;
   float reflection = envGrey * ppEyeDiff * 0.1;
   float reflectionRim = envGrey * ppEyeFres * 0.3;
   float fres = ppEyeFres * 2.2;
   float lighting = ( envSpec - ppEyeFres ) * 0.1;
   float centerGlow = ppEyeDiff * 0.4 * bloomShadow;
   
   vec3 litRoomColor = vec3( reflectionRim + fres + centerGlow);
   vec3 darkRoomColor = vec3( ppEyeDiff + 0.5 ) * 0.7 + color * 0.4 + 0.4 + pow( ppEyeFres, 2.0 ) * 0.4;
   vec3 cap = mix( litRoomColor, darkRoomColor * spectrumCol, mainPower );
   	
  
   // ====== FOG =====
   float fogDistance = gl_FragCoord.z / gl_FragCoord.w;
   float fogOpacity= fog_exp2(fogDistance, FOG_DENSITY);
   
   vec3 fogColor = vec3(250.0/255.0, 200.0/255.0, 0.0/255.0);
   //vec3 fogColor = vec3(100.0 / 255.0, 100.0 / 255.0, 100.0 / 255.0);
   
   const float MIN_Y = 0.05;
   float opacity = smoothstep(0.0, MIN_Y, abs(vPosition.y));
   

   
   vec4 fogCol = mix(vec4(1.0,1.0,1.0,1.0),vec4(fogColor,1.) + vec4(fogAmount),fogOpacity);
   glFragColor = mix(vec4(cap,1.),fogCol,0.5);
}