
// fragment shader lighting functions based on @flight404's technique found in eyeo 2012 code
// first function accepts normals, second one attempts to calculate normals based on
// this thread : 
// https://github.com/mrdoob/three.js/issues/6117

vec3 calcLighting(vec3 vPosition,vec3 vNormal, vec3 vEyeDir){
    vec3 lightPos = vec3( 0.0, 10.0, 0.0 );
    vec3 lightDir = lightPos - vPosition;
    vec3 lightDirNorm = normalize( lightDir );
    vec3 spectrumCol = vec3(1.0,0.5,0.0);
    
    
    float ppDiff = max( dot( vNormal, lightDirNorm ), 0.0 );
    vec3 HV = normalize( lightDirNorm + vEyeDir );
    float NdotHV = max( dot( vNormal, HV ), 0.0 );
    float ppSpec = pow( NdotHV, 30.0 );
    float ppFres = pow( 1.0 - ppDiff, 2.5 );
    float ppEyeDiff = max( dot( vNormal, vEyeDir ), 0.0 );
    float ppEyeFres = pow( 1.0 - ppEyeDiff, 2.0 );
    vec3 c = vec3( 1.0 - ppEyeFres * 0.02 );
    vec3 reflectDir = reflect( vEyeDir, vNormal * vec3( 1.0, 1.5, 1.0 ) );
    
    vec3 envColor = vec3(1.0,1.0,0.0);
    float envGrey = envColor.r;
    float envSpec = envColor.g;
    float fakeAo = pow( vNormal.y * 0.3 + 0.3, 0.4 );
    float rimLight = pow( fakeAo, 4.0 ) * ppEyeFres;
    float bloomShadow = vPosition.y * 0.005 + 0.8;
    float reflection = envGrey * ppEyeDiff * 0.1;
    float reflectionRim = envGrey * ppEyeFres * 0.3;
    float fres = ppEyeFres * 0.2;
    float lighting = ( envSpec - ppEyeFres ) * 0.1;
    float centerGlow = ppEyeDiff * 0.4 * bloomShadow;
    
    vec3 litRoomColor = vec3( reflectionRim + fres);
    vec3 darkRoomColor = vec3( ppEyeDiff + 0.5 ) * 0.7 + color * 0.4 + 0.4 + pow( ppEyeFres, 2.0 ) * 0.4;
    vec3 cap = mix( litRoomColor, darkRoomColor * spectrumCol, mainPower );
    
    return cap;
}
vec3 calcLighting(vec3 vPosition, vec3 vEyeDir){
    vec3 lightPos = vec3( 0.0, 10.0, 0.0 );
    vec3 lightDir = lightPos - vPosition;
    vec3 lightDirNorm = normalize( lightDir );
    vec3 spectrumCol = vec3(1.0,0.5,0.0);
    
    // calculate normal
    //https://github.com/mrdoob/three.js/issues/6117
    vec3 fdx = vec3(dFdx(vPosition.x),dFdx(vPosition.y),dFdx(vPosition.z));
    vec3 fdy = vec3(dFdy(vPosition.x),dFdy(vPosition.y),dFdy(vPosition.z));
    vec3 vNormal = normalize(cross(fdx,fdy));
    
    
    float ppDiff = max( dot( vNormal, lightDirNorm ), 0.0 );
    vec3 HV = normalize( lightDirNorm + vEyeDir );
    float NdotHV = max( dot( vNormal, HV ), 0.0 );
    float ppSpec = pow( NdotHV, 30.0 );
    float ppFres = pow( 1.0 - ppDiff, 2.5 );
    float ppEyeDiff = max( dot( vNormal, vEyeDir ), 0.0 );
    float ppEyeFres = pow( 1.0 - ppEyeDiff, 2.0 );
    vec3 c = vec3( 1.0 - ppEyeFres * 0.02 );
    vec3 reflectDir = reflect( vEyeDir, vNormal * vec3( 1.0, 1.5, 1.0 ) );
    
    vec3 envColor = vec3(1.0,1.0,0.0);
    float envGrey = envColor.r;
    float envSpec = envColor.g;
    float fakeAo = pow( vNormal.y * 0.3 + 0.3, 0.4 );
    float rimLight = pow( fakeAo, 4.0 ) * ppEyeFres;
    float bloomShadow = vPosition.y * 0.005 + 0.8;
    float reflection = envGrey * ppEyeDiff * 0.1;
    float reflectionRim = envGrey * ppEyeFres * 0.3;
    float fres = ppEyeFres * 0.2;
    float lighting = ( envSpec - ppEyeFres ) * 0.1;
    float centerGlow = ppEyeDiff * 0.4 * bloomShadow;
    
    vec3 litRoomColor = vec3( reflectionRim + fres);
    vec3 darkRoomColor = vec3( ppEyeDiff + 0.5 ) * 0.7 + color * 0.4 + 0.4 + pow( ppEyeFres, 2.0 ) * 0.4;
    vec3 cap = mix( litRoomColor, darkRoomColor * spectrumCol, mainPower );
    
    return cap;
}