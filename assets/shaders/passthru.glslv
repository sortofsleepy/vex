#version 150

uniform mat4 ciModelViewProjection;
in vec3 ciPosition;

void main(){
   
    gl_Position = ciModelViewProjection * vec4(ciPosition,1.);
  
}