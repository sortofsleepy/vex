#version 150

uniform vec2 dir;
uniform vec2 resolution;
uniform sampler2D mInput;
out vec4 glFragColor;
in vec2 vUv;

vec4 blur9(sampler2D image, vec2 uv, vec2 res, vec2 direction) {
  vec4 color = vec4(0.0);
  vec2 off1 = vec2(1.3846153846) * direction;
  vec2 off2 = vec2(3.2307692308) * direction;
  color += texture(image, uv) * 0.2270270270;
  color += texture(image, uv + (off1 / res)) * 0.3162162162;
  color += texture(image, uv - (off1 / res)) * 0.3162162162;
  color += texture(image, uv + (off2 / res)) * 0.0702702703;
  color += texture(image, uv - (off2 / res)) * 0.0702702703;
  return color;
}

void main(){
     vec4 dat = texture(mInput,vUv);
     glFragColor = blur9(mInput,vUv,resolution,vec2(2.0,2.0));
}