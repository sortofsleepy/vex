#version 150
uniform samplerCube uRadianceMap;
uniform samplerCube uIrradianceMap;
uniform sampler2D uEnvTex;
uniform sampler2D uGradientTex;
uniform vec3 uBaseColor;
uniform float uRoughNess;
uniform float uRoughness4;
uniform float uMetallic;
uniform float uSpecular;
uniform vec4 shadeColor;

uniform float uExposure;
uniform float uGamma;

in vec2 vUv;
in vec3 vNormal;
in vec3 vPosition;
in vec3 vEyePosition;
in vec3 vWsNormal;
in vec3 vWsPosition;

#define saturate(x) clamp(x,0.0,1.0);
#define PI 3.14159;


// Filmic tonemapping from
// http://filmicgames.com/archives/75

const float A = 0.15;
const float B = 0.50;
const float C = 1.80;
const float D = 0.20;
const float E = 0.02;
const float F = 1.30;


#include "utils.glslv"
out vec4 glFragColor;


void main(){
    vec4 env = texture(uEnvTex,vUv);
    vec4 gradient = texture(uGradientTex,vUv);
    vec3 N = normalize(vWsNormal);
    vec3 V = normalize(vEyePosition);
    
    vec3 color = getPbr(N,V,uBaseColor,uRoughNess,uMetallic,uSpecular);
    color = Uncharted2Tonemap(color * uExposure);
    color = color * (1.0 / Uncharted2Tonemap(vec3(20.0)));
    
    color = pow(color,vec3(1.0 / uGamma));
    float g = (color.r + color.g + color.b) / 3.0;
    g = pow(g,9.0);
    
    // a color to mix into the thing
    vec4 mixColor = vec4(1.0,0.5,0.0,1.0);
    
    if(shadeColor.x > 0.0){
        mixColor = shadeColor;
    }

    vec4 finalColor = vec4(1.0,1.0,1.0,g * 0.5) * mixColor;
    finalColor = mix(finalColor,env,0.8);
    finalColor = mix(gradient,finalColor,0.8);
    glFragColor = mix(finalColor,gradient,0.3);
}