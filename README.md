Vex
====

Just a small test of a couple of different rendering techniques. 
Built with Cinder

Building
=== 
* You'll need to pull from the latest master from Cinder and build manually as I don't think there are any release builds 
quite yet. Please see the site for details.

* You'll also need CMake. Currently set for 3.0 up but the build instructions for the project itself is not terribly complex so lower
versions may work too.

* Adjust the path to your Cinder installation as necessary in the `CMakeLists.txt` file.
